import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class TextFileReader {
	public static void main(String args[]) throws IOException {
		String FileNameWithPath = "C:\\Users\\Gaurav\\eclipse-workspace\\javaprogram\\CoreJava\\src\\Pledge";

		File TextFile = new File(FileNameWithPath);
		FileReader fr = new FileReader(TextFile);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		line = br.readLine();
		System.out.println(line);
		line = br.readLine();
		System.out.println(line);
		line = br.readLine();
		System.out.println(line);
		line = br.readLine();
		System.out.println(line);
	}
}
