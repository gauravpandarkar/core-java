package collection_framework;

public class ArrayMain {
	public static void main(String args[]) {
		int a=10;
		TextFile std=new TextFile();
		int b[]=new int[5];
		b[0]=6;
		b[1]=8;
		b[2]=9;
		b[3]=10;
		b[4]=12;
		System.out.println("Sum="+(b[4]+b[3]));
		System.out.println("*********While Loop********");
		int i=0;
		while(i<5) {
			System.out.println(b[i]);
			i=i+1;
			
		}
		System.out.println("*******For Loop********");
		for(i=0;i<5;i=i+1){
			System.out.println(b[i]);
		}
		//enhanced for
		for(int k:b) {
			System.out.println(k);
			
		}
	}

}
