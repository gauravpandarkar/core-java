
public class BinomialExpression {
	public static void main(String args[]) {
		equation1(20, 50);
		equation2(20, 40);
		equation3(49, 69);

	}

	public static void equation1(int a, int b) {
		int c = a * a + 2 * a * b + b * b;
		System.out.println("Solution of equation1 is=" + c);
	}

	public static void equation2(int a, int b) {
		int c = a * a * a + 3 * a * a * b + 3 * a * b * b + b * b * b;
		System.out.println("Solution of equation2 is" + c);
	}

	public static void equation3(int a, int b) {
		int c = a * a * a * a + 4 * a * a * a * b + 6 * a * a * b * b + 4 * a * b * b * b + b * b * b * b;
		System.out.println("Solution of equation3 is=" + c);
	}

}
