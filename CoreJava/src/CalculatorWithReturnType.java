
public class CalculatorWithReturnType {
	public static void main(String args[]) {
		int a = addition(45, 57);
		System.out.println("Addition is =" + a);
		a = substraction(65, 76);
		System.out.println("Substraction is=" + a);
		a = multiplication(24, 54);
		System.out.println("Multiplication is=" + a);
		float b = division(34, 54);
		System.out.println("Division is=" + b);
		a = square(20);
		System.out.println("Square is=" + a);
		a = cube(40);
		System.out.println("Cube is=" + a);
		String c = concat("hello", "Java");
		System.out.println("Concatination of strings is=" + c);

	}

	public static int addition(int a, int b) {
		int c = a + b;
		return c;
	}

	public static int substraction(int a, int b) {
		int c = a - b;
		return c;
	}

	public static int multiplication(int a, int b) {
		int c = a * b;
		return c;
	}

	public static float division(int a, int b) {
		float c = a / b;
		return c;
	}

	public static int square(int a) {
		int c = a * a;
		return c;
	}

	public static String concat(String a, String b) {
		String c = a + " " + b;
		return c;
	}

	public static int cube(int a) {
		int c = a * a * a;
		return c;
	}

}
