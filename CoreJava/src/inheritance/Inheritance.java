package inheritance;

public class Inheritance {
	public static void main(String args[]) {
		Cat c=new Cat();
		c.name="Murphy";
		c.jumpInMeter="3";
		c.printName();
		c.printJumpInMeter();
		Dog d=new Dog();
		d.name="Moti";
		d.noOfNails=17;
		d.printName();
		d.printNoOFNails();
		Cow cw=new Cow();
		cw.name="Kapili";
		cw.milkQty="12";
		cw.printName();
		cw.printMilkQty();
	}

}
