import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class CsvFileReader {
	public static void main(String args[]) throws IOException {
		String FileNameWithPath = "C:\\Users\\Gaurav\\eclipse-workspace\\javaprogram\\CoreJava\\src\\Information.csv";
		File CsvFile = new File(FileNameWithPath);
		FileReader fr = new FileReader(CsvFile);
		BufferedReader br = new BufferedReader(fr);
		String line = "";
		while (line != null) {
			line = br.readLine();
			System.out.println(line);
		}

	}

}
